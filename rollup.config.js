import babel from "rollup-plugin-babel";
import compiler from "@ampproject/rollup-plugin-closure-compiler";
import rollupTypescript from 'rollup-plugin-typescript2';

export default {
    input: "src/index.ts",
    output: [{
        file: "dist/index.js",
        format: "umd",
        name: "AMapLoader",
    },{
        file: "dist/index.cjs",
        format: "cjs",
        sourcemap: true
    },{
        file: "dist/index.mjs",
        format: "es",
        sourcemap: true
    }],
    plugins: [
        rollupTypescript(),
        babel({
            presets: [["@babel/env", { targets: { ie: 9 } }]],
        }),
        compiler(),
    ],
};
