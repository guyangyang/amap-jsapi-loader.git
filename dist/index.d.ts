interface LoadOption {
    key: string;
    version?: string;
    plugins?: string[];
    AMapUI?: {
        version?: string;
        plugins?: string[];
    };
    Loca?: {
        version?: string;
    };
}
declare function reset(): void;
declare const _default: {
    load: (options: LoadOption) => Promise<unknown>;
    reset: typeof reset;
};
export default _default;
